package com.mixtelematics.mixcoding.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mixtelematics.mixcoding.R;
import com.mixtelematics.mixcoding.usecase.CalculateResultsAsync;
import com.mixtelematics.mixcoding.usecase.CalculateSpeed;
import com.mixtelematics.mixcoding.usecase.ReadInputFile;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, CalculateResultsAsync.Listener {

    Button btnStart;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = findViewById(R.id.btnStart);
        progressBar = findViewById(R.id.progressBar);

        btnStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnStart) {
            showProgressBarIndicator();
            btnStart.setEnabled(false);
            new CalculateResultsAsync(this,this).execute();
        }
    }

    private void showProgressBarIndicator() {
        progressBar.setVisibility(View.VISIBLE);
    }
    private void hideProgressBarIndicator() {
        btnStart.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onComplete(List<String> drivers) {
        hideProgressBarIndicator();
        Toast.makeText(this,"Calculations Completed", Toast.LENGTH_LONG).show();
    }
}