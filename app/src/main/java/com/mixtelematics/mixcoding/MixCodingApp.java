package com.mixtelematics.mixcoding;

import android.app.Application;

import com.mixtelematics.mixcoding.usecase.ReadInputFile;

public class MixCodingApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ReadInputFile readInputFile = new ReadInputFile(this);
        readInputFile.processAllFiles();
    }
}
