package com.mixtelematics.mixcoding.models;

public class Vehicle {

    short vehicleId;
    String description;
    String registrationNumber;


    public Vehicle() {
    }

    public Vehicle(short vehicleId, String description, String registrationNumber) {
        this.vehicleId = vehicleId;
        this.description = description;
        this.registrationNumber = registrationNumber;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(short vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
