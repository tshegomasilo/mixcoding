package com.mixtelematics.mixcoding.models;

import java.util.Date;

public class VehicleDriverIdentification {

    Date timeOfIdentification;
    int vehicleId;
    int driverId;

    public VehicleDriverIdentification() {

    }

    public VehicleDriverIdentification(Date timeOfIdentification, int vehicleId, int driverId) {
        this.timeOfIdentification = timeOfIdentification;
        this.vehicleId = vehicleId;
        this.driverId = driverId;
    }

    public Date getTimeOfIdentification() {
        return timeOfIdentification;
    }

    public void setTimeOfIdentification(Date timeOfIdentification) {
        this.timeOfIdentification = timeOfIdentification;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }
}
