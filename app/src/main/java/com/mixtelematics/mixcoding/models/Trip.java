package com.mixtelematics.mixcoding.models;

public class Trip {

    short vehicleId;
    long tripStart;
    long tripEnd;
    float startOdometer;
    float endOdometer;
    float distance;
    float speed;

    public Trip() {

    }

    public Trip(short vehicleId, long tripStart, long tripEnd, float startOdometer, float endOdometer, float distance) {
        this.vehicleId = vehicleId;
        this.tripStart = tripStart;
        this.tripEnd = tripEnd;
        this.startOdometer = startOdometer;
        this.endOdometer = endOdometer;
        this.distance = distance;
    }

    public short getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(short vehicleId) {
        this.vehicleId = vehicleId;
    }

    public long getTripStart() {
        return tripStart;
    }

    public void setTripStart(long tripStart) {
        this.tripStart = tripStart;
    }

    public long getTripEnd() {
        return tripEnd;
    }

    public void setTripEnd(long tripEnd) {
        this.tripEnd = tripEnd;
    }

    public float getStartOdometer() {
        return startOdometer;
    }

    public void setStartOdometer(float startOdometer) {
        this.startOdometer = startOdometer;
    }

    public float getEndOdometer() {
        return endOdometer;
    }

    public void setEndOdometer(float endOdometer) {
        this.endOdometer = endOdometer;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
