package com.mixtelematics.mixcoding.usecase;

import static com.mixtelematics.mixcoding.usecase.ReadInputFile.getDriverHashMap;
import static com.mixtelematics.mixcoding.usecase.ReadInputFile.getTrips;
import static com.mixtelematics.mixcoding.usecase.ReadInputFile.getVehicleDriverIdentificationList;

import com.mixtelematics.mixcoding.models.Driver;
import com.mixtelematics.mixcoding.models.Trip;
import com.mixtelematics.mixcoding.models.VehicleDriverIdentification;

import java.util.ArrayList;
import java.util.List;

public class CalculateSpeed {

    ReadInputFile readInputFile;

    public CalculateSpeed(ReadInputFile readInputFile) {
        this.readInputFile = readInputFile;
    }

    private static float calculateHighestAverageSpeed(ArrayList<Trip> trips){

        float averageSpeed;

        averageSpeed = calculateTotalSpeed(trips) / calculateNumberOfTrips(trips);

        return averageSpeed;

    }

    /**
     * This finds the top 10 drivers with highest average speed
     * @return list of drivers
     */
    public static List<String> getDriverHighestAverageSpeed(){
        ArrayList<String> drivers = new ArrayList<>();
        float highestAverageSpeed = calculateHighestAverageSpeed(getTrips());
        ArrayList<Trip> filteredTrips = getTrips();
        filteredTrips.removeIf(trip -> trip.getSpeed() > highestAverageSpeed);

        ArrayList<VehicleDriverIdentification> vdiList = getVehicleDriverIdentificationList();

        for(Trip trip : filteredTrips){

            for(VehicleDriverIdentification vdi : vdiList){
                if(trip.getVehicleId() == vdi.getVehicleId() &&
                        (vdi.getTimeOfIdentification().getTime() <= trip.getTripEnd()) &&
                        trip.getTripStart() >= vdi.getTimeOfIdentification().getTime()){
                    Driver driver = getDriverHashMap().get(vdi.getDriverId());
                    if (driver != null) {
                        drivers.add(driver.getDriverName());
                    }
                }
            }
        }

        return drivers;

    }

    private static float calculateTotalSpeed(ArrayList<Trip> trips){
        final float[] speedTotal = {0F};
        trips.forEach(trip ->{
            float speed = calculateSpeed(trip.getDistance(), (trip.getTripStart() - trip.getTripEnd()));
            trip.setSpeed(speed);
            speedTotal[0] += speed;
        });

        return speedTotal[0];
    }

    public static float calculateSpeed(float distance, long time){
        return distance / time;
    }

    public static int calculateNumberOfTrips(ArrayList<Trip> trips){
        return trips.size();
    }
}
