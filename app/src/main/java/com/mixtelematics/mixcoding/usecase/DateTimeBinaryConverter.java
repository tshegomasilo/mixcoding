package com.mixtelematics.mixcoding.usecase;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class DateTimeBinaryConverter {

    private static final long NANOS_PER_TICK = 100L;
    private static final long TICKS_PER_SECOND = 1000000000L / NANOS_PER_TICK;
    private static final long YEAR_OFFSET = -62135596800L;

    static Instant fromDateTimeBinary(long fromBytes) {

        // Mask out kind and ticks
        int kind = Math.toIntExact((fromBytes >> 62) & 0x3);
        long ticks = fromBytes & 0x3FFF_FFFF_FFFF_FFFFL;
        LocalDateTime cSharpEpoch = LocalDate.of(1, Month.JANUARY, 1).atStartOfDay();
        // 100 nanosecond units or 10^-7 seconds
        final int unitsPerSecond = 10_000_000 ;
        long seconds = ticks / unitsPerSecond;
        long nanos = (ticks % unitsPerSecond) * 100;
        LocalDateTime ldt = cSharpEpoch.plusSeconds(seconds).plusNanos(nanos);

        switch (kind) {
            case 0: // Unspecified
            case 2: // Local time
                System.out.println("Result LocalDateTime: " + ldt);
                break;

            case 1: // UTC
                OffsetDateTime utcDateTime = ldt.atOffset(ZoneOffset.UTC);
                System.out.println("Result OffsetDateTime in UTC: " + utcDateTime);
                break;

            default:
                System.out.println("Not a valid DateTimeKind: " + kind);
                break;
        }

        long ticks1 = fromBytes & ((1L << 62) - 1);
        return Instant.ofEpochSecond(ticks1 / TICKS_PER_SECOND + YEAR_OFFSET,
                ticks % TICKS_PER_SECOND * NANOS_PER_TICK);
    }
}
