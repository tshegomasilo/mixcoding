package com.mixtelematics.mixcoding.usecase;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CalculateResultsAsync extends AsyncTask<Void, Void, List<String>> {

    private Context context;
    private Listener listener;

    public interface Listener {
        void onComplete(List<String> drivers);
    }

    public CalculateResultsAsync(Context context, Listener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected List<String> doInBackground(Void... voids) {

        List<String> drivers = CalculateSpeed.getDriverHighestAverageSpeed();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("DriverList.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write("###############################\n");
            outputStreamWriter.write("Date : " + dateFormat.format(date)+"\n");
            outputStreamWriter.write("Driver Name\n\n");
            if (drivers.size() > 0) {
                for (String driverName : drivers) {
                    outputStreamWriter.write(driverName+"\n");
                }
            } else {
                outputStreamWriter.write("No Driver's found..........\n");
            }
            outputStreamWriter.write("\n###############################");
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }


        return drivers;
    }

    @Override
    protected void onPostExecute(List<String> drivers) {
        super.onPostExecute(drivers);
        listener.onComplete(drivers);
    }
}
