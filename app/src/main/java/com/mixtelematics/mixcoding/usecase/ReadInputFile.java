package com.mixtelematics.mixcoding.usecase;

import android.content.Context;
import android.util.Log;

import com.mixtelematics.mixcoding.models.Driver;
import com.mixtelematics.mixcoding.models.Trip;
import com.mixtelematics.mixcoding.models.Vehicle;
import com.mixtelematics.mixcoding.models.VehicleDriverIdentification;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ReadInputFile {

    private static final String TAG = ReadInputFile.class.getSimpleName();
    private static final String FILE_TRIPS = "Trips.dat";
    private static final String FILE_DRIVERS = "DriverList.csv";
    private static final String FILE_VEHICLE= "VehicleList.csv";
    private static final String FILE_VEHICLE_DRIVER_ID= "VehicleDriverIdentifications.txt";
    private static final String DATE_TIME_FORMAT = "yyy-MM-dd HH:mm:ss";
    private Context context;
    private static HashMap<Integer, Driver> driverHashMap = new HashMap<>();
    private HashMap<Integer, Vehicle> vehicleHashMap = new HashMap<>();
    private static ArrayList<VehicleDriverIdentification> vehicleDriverIdentificationList = new ArrayList<>();
    private static ArrayList<Trip> trips = new ArrayList<>();

    public ReadInputFile(Context context) {
        this.context = context;
    }

    public ReadInputFile() {

    }

    public void processAllFiles(){
        readTripsBinaryFile();
        readDriverListFile();
        readVehicleListFile();
        readVehicleDriverIdentificationFile();
    }

    private void readVehicleDriverIdentificationFile() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(FILE_VEHICLE_DRIVER_ID)));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                String dateTime = line.substring(0,line.indexOf("(")- 1).trim();
                line = line.substring(dateTime.length());

                String vehicleId = line.substring(line.indexOf("(") + 1,line.indexOf(":")).trim();
                line = line.substring(vehicleId.length());
                String driverId = line.substring(line.lastIndexOf(":") + 1, line.length() - 1).trim();

                VehicleDriverIdentification vdi = new VehicleDriverIdentification();
                vdi.setTimeOfIdentification(parseDate(dateTime));
                vdi.setDriverId(Integer.parseInt(driverId));
                vdi.setVehicleId(Integer.parseInt(vehicleId));

                vehicleDriverIdentificationList.add(vdi);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Date parseDate(String dateTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
        try {
            return simpleDateFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void readDriverListFile() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(FILE_DRIVERS)));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                String[] driverLine = line.split(",");
                Driver driver = new Driver(Integer.parseInt(driverLine[0]),driverLine[1]);
                driverHashMap.put(driver.getDriverId(),driver);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void readVehicleListFile() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(FILE_VEHICLE)));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                String[] vehicleLine = line.split(",");
                Vehicle vehicle = new Vehicle();
                vehicle.setVehicleId(Short.parseShort(vehicleLine[0]));
                vehicle.setDescription(vehicleLine[1]);
                vehicle.setRegistrationNumber(vehicleLine[2]);
                vehicleHashMap.put(vehicle.getVehicleId(),vehicle);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void readTripsBinaryFile(){
        try {
            DataInputStream dis = new DataInputStream(new BufferedInputStream(context.getAssets().open(FILE_TRIPS)));

            while (dis.available()>0){

                // Read the first data
                short vehicleId = (short) Math.abs(dis.readShort());
                long tripStart = Math.abs(Date.from(DateTimeBinaryConverter.fromDateTimeBinary(dis.readLong())).getTime());
                float startOdometer = Math.abs(dis.readFloat());
                long tripEnd = Math.abs(Date.from(DateTimeBinaryConverter.fromDateTimeBinary(dis.readLong())).getTime());
                float endOdometer = Math.abs(dis.readFloat());
                float distance = Math.abs(dis.readFloat());

                Trip trip = new Trip();
                trip.setVehicleId(vehicleId);
                trip.setTripStart(tripStart);
                trip.setTripEnd(tripEnd);
                trip.setStartOdometer(startOdometer);
                trip.setEndOdometer(endOdometer);
                trip.setDistance(distance);

                trips.add(trip);

                Log.d(TAG,"Vehicle ID : "+vehicleId + "    Trip Start : "+ tripStart +
                        "   StartOdometer : " + startOdometer + "   TripEnd : " + tripEnd +
                        "   EndOdometer : " + endOdometer + "   Distance : "+ distance);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static HashMap<Integer, Driver> getDriverHashMap() {
        return driverHashMap;
    }

    public HashMap<Integer, Vehicle> getVehicleHashMap() {
        return vehicleHashMap;
    }

    public static ArrayList<VehicleDriverIdentification> getVehicleDriverIdentificationList() {
        return vehicleDriverIdentificationList;
    }

    public static ArrayList<Trip> getTrips() {
        return trips;
    }
}
