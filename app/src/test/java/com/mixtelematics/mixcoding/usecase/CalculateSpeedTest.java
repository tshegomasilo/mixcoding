package com.mixtelematics.mixcoding.usecase;

import static org.hamcrest.CoreMatchers.is;

import android.content.Context;

import com.mixtelematics.mixcoding.models.Trip;

import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class CalculateSpeedTest {

    CalculateSpeed SUT;
    ReadInputFile readInputFile;
    ArrayList<Trip> trips;


    @Before
    public void setUp() throws Exception {
        readInputFile = new ReadInputFile();
        SUT = new CalculateSpeed(readInputFile);
        trips = new ArrayList<>();
        Trip trip1 = new Trip((short) 1224,1625079466,1625080466,4,5,600);
        Trip trip2 = new Trip((short) 1225,1623079466,1623080466,10,16,700);
        trips.add(trip1);
        trips.add(trip2);
    }

    @Test
    public void calculateNumberOfTrips_TwoReturned() {
        int result = SUT.calculateNumberOfTrips(trips);
        MatcherAssert.assertThat(result,is(2));
    }

    @Test
    public void calculateSpeed_TwoReturned() {
        int speed = (int) SUT.calculateSpeed(100, 2); //100KM for 2Hours
        MatcherAssert.assertThat(speed,is(50));
    }
}