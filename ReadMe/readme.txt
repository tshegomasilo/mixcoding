Below is the path in the emulator that write the file output.

/data/data/com.mixtelematics.mixcoding/files/DriverList.txt


Steps
1, Launch the App
2, Press Start button
3, A Toast will be displayed when calculations are completed.
4. An outputfile will be created called DriverList.txt
5, Here's the path of the file : 
   
   /data/data/com.mixtelematics.mixcoding/files/DriverList.txt
